import os

# Configurações básicas
#  https://wiki.archlinux.org/index.php/Installation_guide_(Português)

frase = 'Aplicando as configurações básicas'
print('=' * len(frase))
print(frase)
print('=' * len(frase))

os.system('ln -sf /usr/share/zoneinfo/America/Sao_Paulo /etc/localtime')
os.system('hwclock --systohc')
os.system("sed -i '178s/.//' /etc/locale.gen")
os.system("sed -i '394s/.//' /etc/locale.gen")
os.system('locale-gen')
os.system('echo "LANG=pt_BR.UTF-8" >> /etc/locale.conf')
os.system('echo "KEYMAP=br-abnt2" >> /etc/vconsole.conf')
os.system('echo "arch" >> /etc/hostname')
os.system('echo "127.0.0.1 localhost.localdomain localhost" >> /etc/hosts')
os.system('echo "::1 localhost.localdomain localhost" >> /etc/hosts')
os.system('echo "127.0.1.1 arch.localdomain arch" >> /etc/hosts')

# Configuração do pacman

frase = 'Deseja usar as configurações otimizadas? [s/n]:'
print('=' * len(frase))

loop = 0
while loop == 0:
    resp = str(input(frase))

    if resp == 's' or resp == 'S':
        os.system('mv /etc/pacman.conf /etc/pacman.conf.backup')
        os.system('cp pacman.conf /etc/')
        os.system('pacman -Syu --noconfirm')
        print('Aplicada as configurações otimizadas')
        loop = 1

    if resp == 'n' or resp == 'N':
        print('Aplicada as configurações padrão')
        loop = 1

print('=' * len(frase))
print('''Serão instalados os seguintes programas:
    -: inicialização do sistema (grub, efibootmgr, ntfs-3g, os-prober);
    -: gerenciador de internet (networkmanager);
    -: gerenciador de bluetooth (bluez, bluez-utils, pulseaudio-bluetooth);
    -: firewall (firewalld);
    -: interpretador de terminal (zsh);
    -: administrador do sistema (sudo);
    -: editor de texto via terminal (nano);
    -: atualização de microcódigo (intel-ucode ou amd-ucode).''')
print('')

a = str(input('Pressione qualquer tecla para continuar'))

frase = 'INSTALANDO OS PACOTES NECESSÁRIOS'
print('=' * len(frase))
print(frase)
print('=' * len(frase))

os.system('pacman -S --noconfirm grub efibootmgr ntfs-3g os-prober networkmanager bluez bluez-utils pulseaudio-bluetooth firewalld zsh sudo nano intel-ucode')

frase = 'CONFIGURANDO A INICIALIZAÇÃO DO SISTEMA'
print('=' * len(frase))
print(frase)
print('=' * len(frase))

os.system('grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=Arch --recheck')
os.system('grub-mkconfig -o /boot/grub/grub.cfg')

frase = 'CONFIGURAÇÃO DO USUÁRIO'
print('=' * len(frase))
print(frase)
print('=' * len(frase))

user = input('Digite o nome de usuário:  ')
senha = input('Digite a senha:  ')

os.system(f'useradd -m {user}')
os.system(f'echo user:{senha} | chpasswd')
os.system(f'echo "{user} ALL=(ALL) ALL" >> /etc/sudoers')

frase = 'Instalando pacotes finais'
print('=' * len(frase))
print(frase)
print('=' * len(frase))

os.system('pacman -S --needed --noconfirm base-devel')